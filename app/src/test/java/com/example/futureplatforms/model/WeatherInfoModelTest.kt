package com.example.futureplatforms.model

import org.junit.Test

class WeatherInfoModelTest {

    @Test
    fun getID() {
    }

    @Test
    fun setID() {
    }

    @Test
    fun getCurrentConditions() {

    }

    @Test
    fun setCurrentConditions() {
    }

    @Test
    fun getTemperature() {
    }

    @Test
    fun setTemperature() {
    }

    @Test
    fun getWindSpeed() {
    }

    @Test
    fun setWindSpeed() {
    }

    @Test
    fun getWindDirection() {
    }

    @Test
    fun setWindDirection() {
    }

    @Test
    fun getLatitude() {
    }

    @Test
    fun setLatitude() {
    }

    @Test
    fun getLongitude() {
    }

    @Test
    fun setLongitude() {
    }

    @Test
    fun getLastUpdatedDate() {
    }

    @Test
    fun setLastUpdatedDate() {
    }

    @Test
    fun getWeatherIcon() {
    }

    @Test
    fun setWeatherIcon() {
    }
}