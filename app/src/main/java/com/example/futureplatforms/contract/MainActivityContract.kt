package com.example.futureplatforms.contract

import com.example.futureplatforms.BasePresenter
import com.example.futureplatforms.BaseView

interface MainActivityContract {
    interface View: BaseView<Presenter?> {
        fun showWeatherInfo()
        fun showErrorMessage(message: String)
    }

    interface Presenter : BasePresenter {
        fun getWeatherInfo(lat: Double, lng: Double, appId: String)
    }
}