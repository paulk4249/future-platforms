package com.example.futureplatforms.util

import android.content.Context
import android.net.ConnectivityManager

object RemoteUtils {
    @JvmStatic
    fun isOnline(context: Context): Boolean {
        val connectManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val netInfo = connectManager.activeNetworkInfo
        return netInfo != null && netInfo.isConnected
    }
}