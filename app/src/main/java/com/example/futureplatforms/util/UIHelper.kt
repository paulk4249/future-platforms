package com.example.futureplatforms.util

import android.app.Activity
import android.app.ProgressDialog

class UIHelper(private val activity : Activity?) {
    private var loadingDialog : ProgressDialog? = null

    fun showLoadingDialog(message : String?) {
        activity?.runOnUiThread { loadingDialog = ProgressDialog.show(activity, "", message)}
    }

    fun dismissLoadingDialog() {
        if (activity != null && loadingDialog != null) {
            loadingDialog!!.dismiss()
        }
    }
}