package com.example.futureplatforms.util

import java.text.SimpleDateFormat
import java.util.*

object CalendarUtils {
    fun getDateTime(d: Date): String {
        val dateFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault())
        return dateFormat.format(d)
    }

    fun getDate(d: Date): String {
        val dateFormat = SimpleDateFormat("dd-MM-yyyy HH:mm", Locale.getDefault())
        return dateFormat.format(d)
    }
}