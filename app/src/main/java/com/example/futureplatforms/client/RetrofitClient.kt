package com.example.futureplatforms.client

import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query
import java.util.concurrent.TimeUnit

class RetrofitClient {
    companion object {
        private var BASE_URL = "https://api.openweathermap.org/"
        private var retrofit: Retrofit? = null

        @JvmStatic
        val retrofitInstance: Retrofit?
            get() {
                val client = OkHttpClient.Builder()
                client.connectTimeout(10, TimeUnit.SECONDS)
                if (retrofit == null) {
                    val gson = GsonBuilder()
                        .setDateFormat("yyyy-MM-dd'T'HH:mm:ss")
                        .create()
                    retrofit = Retrofit.Builder()
                        .baseUrl(BASE_URL)
                        .addConverterFactory(GsonConverterFactory.create(gson))
                        .client(client.build())
                        .build()
                }
                return retrofit
            }
    }

    interface GetWeatherInfoByLatAndLong {
        @GET("data/2.5/weather?")
        fun getWeatherInfo(
            @Query("lat") latitude: String,
            @Query("lon") longitude: String,
            @Query("units") metric: String,
            @Query("appid") apiKey: String
        ): Call<ResponseBody>
    }
}