package com.example.futureplatforms.repoContract

interface MainActivityRepoContract {

    interface GetSyncCallback {
        fun syncCompleted()
        fun syncInterrupted(message: String)
    }
}