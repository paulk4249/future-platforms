package com.example.futureplatforms.model

import com.j256.ormlite.field.DataType
import com.j256.ormlite.field.DatabaseField
import com.j256.ormlite.table.DatabaseTable
import java.util.*

@DatabaseTable(tableName = WeatherInfoModel.TABLE_NAME)
class WeatherInfoModel {

    @DatabaseField(columnName = FIELD_NAME_ID)
    var iD: Int = 0

    @DatabaseField(columnName = FIELD_NAME_CURRENT_CONDITIONS)
    var currentConditions: String = ""

    @DatabaseField(columnName = FIELD_NAME_TEMPERATURE)
    var temperature: Double = 0.0

    @DatabaseField(columnName = FIELD_NAME_WIND_SPEED)
    var windSpeed: Double = 0.0

    @DatabaseField(columnName = FIELD_NAME_WIND_DIRECTION)
    var windDirection: String = ""

    @DatabaseField(columnName = FIELD_NAME_LATITUDE)
    var latitude: Double = 0.0

    @DatabaseField(columnName = FIELD_NAME_LONGITUDE)
    var longitude: Double = 0.0

    @DatabaseField(columnName = FIELD_NAME_LAST_UPDATED_DATE, dataType = DataType.DATE_STRING, format = "yyyy-MM-dd HH:mm:ss")
    var lastUpdatedDate: Date? = null

    @DatabaseField(columnName = FIELD_NAME_ICON)
    var weatherIcon: String = ""

    constructor()
    constructor(conditions: String, temp: Double, wS: Double, wD: String, lt: Double, lng: Double, icn: String) {
        this.iD = 1
        this.currentConditions = conditions
        this.temperature = temp
        this.windSpeed = wS
        this.windDirection = wD
        this.latitude = lt
        this.longitude = lng
        this.weatherIcon = icn
        this.lastUpdatedDate = Date()
    }

    companion object {
        const val TABLE_NAME = "WeatherInfo"
        const val FIELD_NAME_ID = "_id"
        const val FIELD_NAME_CURRENT_CONDITIONS = "CurrentConditions"
        const val FIELD_NAME_TEMPERATURE = "Temperature"
        const val FIELD_NAME_WIND_SPEED = "WindSpeed"
        const val FIELD_NAME_WIND_DIRECTION = "WindDirection"
        const val FIELD_NAME_LATITUDE = "Latitude"
        const val FIELD_NAME_LONGITUDE = "Longitude"
        const val FIELD_NAME_ICON = "Icon"
        const val FIELD_NAME_LAST_UPDATED_DATE = "LastUpdatedDate"
    }
}