package com.example.futureplatforms.presenter

import com.example.futureplatforms.contract.MainActivityContract
import com.example.futureplatforms.repoContract.MainActivityRepoContract
import com.example.futureplatforms.repository.MainActivityRepository

class MainActivityPresenter(private val mainActivityRepository: MainActivityRepository, private val mView: MainActivityContract.View) : MainActivityContract.Presenter {

    init {
        mView.setPresenter(this)
    }

    override fun getWeatherInfo(lat: Double, lng: Double, appId: String) {
            mainActivityRepository.callWeatherApi(lat, lng, appId, object : MainActivityRepoContract.GetSyncCallback {
                override fun syncCompleted() {
                    mView.showWeatherInfo()
                }

                override fun syncInterrupted(message: String) {
                    mView.showErrorMessage(message)
                }

            })

    }

    override fun start() {

    }


}