package com.example.futureplatforms

interface BaseView<T> {
    fun setPresenter(presenter: T)
}