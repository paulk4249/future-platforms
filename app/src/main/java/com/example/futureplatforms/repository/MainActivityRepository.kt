package com.example.futureplatforms.repository

import android.content.Context
import com.example.futureplatforms.client.RetrofitClient
import com.example.futureplatforms.coord.Coord
import com.example.futureplatforms.model.WeatherInfoModel
import com.example.futureplatforms.repoContract.MainActivityRepoContract
import com.example.futureplatforms.util.RemoteUtils
import okhttp3.ResponseBody
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.lang.ref.WeakReference

class MainActivityRepository : MainActivityRepoContract {

    fun callWeatherApi(lat: Double, lng: Double, appId: String, callback: MainActivityRepoContract.GetSyncCallback) {
        if (weakReference?.get()?.let { RemoteUtils.isOnline(it) }!!) {
            val service = RetrofitClient.retrofitInstance?.create(RetrofitClient.GetWeatherInfoByLatAndLong::class.java)
            val call = service?.getWeatherInfo(lat.toString(), lng.toString(), "metric", appId)
            call?.enqueue(object : Callback<ResponseBody> {
                override fun onResponse(call: Call<ResponseBody>?, response: Response<ResponseBody>?) {
                    if (response!!.isSuccessful) {
                        try {
                            val jsonObject = JSONObject(response.body().string())
                            // conditions
                            val weatherArray = jsonObject.get("weather") as JSONArray
                            val mainObject = weatherArray.getJSONObject(0) as JSONObject
                            val conditions = mainObject.get("main") //returns conditions element from weather array
                            val icon = mainObject.get("icon") // returns weather icon element from array

                            // temperature
                            val main = jsonObject.get("main") as JSONObject
                            val temp = main.get("temp") // returns temperature from json object

                            //wind
                            val wind = jsonObject.get("wind") as JSONObject
                            val speed = wind.get("speed") // returns wind speed from object
                            val deg = wind.get("deg") // returns wind direction from object
                            val model = WeatherInfoModel(conditions.toString(), temp.toString().toDouble(), speed.toString().toDouble(), getWindDirection(deg.toString().toDouble()), lat, lng, icon.toString())
                            if (weakReference!!.get()?.let {
                                    Coord.createWeatherInfoRecord(model,
                                        it
                                    )
                                } == true) {
                                callback.syncCompleted()
                            } else {
                                callback.syncInterrupted("Error getting weather at this time. Please try again later")
                            }
                        } catch (e: JSONException) {
                            e.printStackTrace()
                            callback.syncInterrupted("Error getting weather at this time. Please try again later")
                        }
                    }
                }

                override fun onFailure(call: Call<ResponseBody>?, t: Throwable?) {
                    if (t != null) {
                        callback.syncInterrupted("Error occurred getting the weather. Please try again later")
                    }
                }
            })
        } else {
            callback.syncInterrupted("You are currently offline. Please connect to internet and try again")
        }
    }

    // here the wind degrees to converted into a direction which is more readable
    fun getWindDirection(degrees: Double): String {
        return when(degrees) {
            in 326.26..11.25 -> "North"
            in 11.26..78.75 -> "North East"
            in 78.76..123.75 -> "East"
            in 123.76..168.75 -> "South East"
            in 168.76..213.75 -> "South"
            in 213.76..258.75 -> "South West"
            in 258.76..303.75 -> "West"
            in 303.76..326.25 -> "North West"
            else -> "N/A"
        }
    }

    companion object {
        @Volatile
        private var instance: MainActivityRepository? = null
        private var weakReference: WeakReference<Context>? = null

        fun getInstance(context: Context): MainActivityRepository? {
            if (instance == null)
                instance = MainActivityRepository()
            weakReference = WeakReference(context)
            return instance
        }
    }
}