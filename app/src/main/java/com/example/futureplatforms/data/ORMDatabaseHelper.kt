package com.example.futureplatforms.data

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import com.example.futureplatforms.model.WeatherInfoModel
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper
import com.j256.ormlite.dao.Dao
import com.j256.ormlite.support.ConnectionSource
import com.j256.ormlite.table.TableUtils
import java.sql.SQLException

class ORMDatabaseHelper(context: Context?) :
    OrmLiteSqliteOpenHelper(context, DATABASE_NAME, null, DATABASE_VERSION) {

    companion object {
        private var instance: ORMDatabaseHelper? = null
        private const val DATABASE_NAME = "MyWeatherApp.db"
        private const val DATABASE_VERSION = 1
        private var weatherInfoModelDao: Dao<WeatherInfoModel, Int>? = null

        @JvmStatic
        @Synchronized
        fun getHelper(context: Context): ORMDatabaseHelper? {
            if (instance == null) instance = ORMDatabaseHelper(context.applicationContext)
            return instance
        }
    }

    override fun onCreate(database: SQLiteDatabase?, connectionSource: ConnectionSource?) {
        try {
            TableUtils.createTable(connectionSource, WeatherInfoModel::class.java)
            database?.execSQL("create unique index idx_weather_id ON ${WeatherInfoModel.TABLE_NAME} (${WeatherInfoModel.FIELD_NAME_ID})")
        } catch (e: SQLException) {
            throw RuntimeException(e)
        }
    }

    override fun onUpgrade(
        database: SQLiteDatabase?,
        connectionSource: ConnectionSource?,
        oldVersion: Int,
        newVersion: Int
    ) {

    }

    @Throws(SQLException::class)
    fun getWeatherInfoDao(): Dao<WeatherInfoModel, Int>? {
        if (weatherInfoModelDao == null)
            weatherInfoModelDao = getDao(WeatherInfoModel::class.java)
        return weatherInfoModelDao
    }

    override fun close() {
        weatherInfoModelDao = null
    }
}