package com.example.futureplatforms.coord

import android.content.Context
import com.example.futureplatforms.data.ORMDatabaseHelper.Companion.getHelper
import com.example.futureplatforms.model.WeatherInfoModel
import com.example.futureplatforms.util.CalendarUtils
import java.sql.SQLException

object Coord {

    fun createWeatherInfoRecord(model: WeatherInfoModel, context: Context): Boolean {
        val db = getHelper(context)?.writableDatabase
        var returnValue = false
        try {
            db?.beginTransaction()
            val objects = arrayOfNulls<Any>(9)
            objects[0] = model.iD
            objects[1] = model.currentConditions
            objects[2] = model.temperature
            objects[3] = model.windSpeed
            objects[4] = model.windDirection
            objects[5] = model.latitude
            objects[6] = model.longitude
            objects[7] = model.weatherIcon
            objects[8] = model.lastUpdatedDate?.let { CalendarUtils.getDateTime(it) }
            db?.execSQL(
                "insert or replace into ${WeatherInfoModel.TABLE_NAME} (${WeatherInfoModel.FIELD_NAME_ID}, ${WeatherInfoModel.FIELD_NAME_CURRENT_CONDITIONS}," +
                        "${WeatherInfoModel.FIELD_NAME_TEMPERATURE}, ${WeatherInfoModel.FIELD_NAME_WIND_SPEED}, ${WeatherInfoModel.FIELD_NAME_WIND_DIRECTION}, ${WeatherInfoModel.FIELD_NAME_LATITUDE}," +
                        "${WeatherInfoModel.FIELD_NAME_LONGITUDE}, ${WeatherInfoModel.FIELD_NAME_ICON}, ${WeatherInfoModel.FIELD_NAME_LAST_UPDATED_DATE}) values (?,?,?,?,?,?,?,?,?) ",
                objects
            )
            db?.setTransactionSuccessful()
            returnValue = true
        } catch (e: SQLException) {
            e.printStackTrace()
        } finally {
            db?.endTransaction()
        }
        return returnValue
    }

    fun isTableEmpty(context: Context): Boolean {
        try {
            val dao = getHelper(context)?.getWeatherInfoDao()
            return dao?.countOf()!! <= 0
        } catch (e: SQLException) {
            throw RuntimeException(e)
        }
    }

    fun getWeatherInfoModel(context: Context): WeatherInfoModel {
        var model = WeatherInfoModel()
        try {
            model = getHelper(context)?.getWeatherInfoDao()?.queryBuilder()
                ?.where()?.eq(WeatherInfoModel.FIELD_NAME_ID, 1)?.queryForFirst()!!
        } catch (e: SQLException) {
            e.printStackTrace()
        }
        return model
    }
}