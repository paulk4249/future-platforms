package com.example.futureplatforms

interface BasePresenter {
    fun start()
}