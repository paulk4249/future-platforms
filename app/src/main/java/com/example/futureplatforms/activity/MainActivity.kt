package com.example.futureplatforms.activity

import android.Manifest
import android.content.pm.PackageManager
import android.location.Location
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import com.bumptech.glide.Glide
import com.example.futureplatforms.R
import com.example.futureplatforms.contract.MainActivityContract
import com.example.futureplatforms.coord.Coord
import com.example.futureplatforms.data.ORMDatabaseHelper.Companion.getHelper
import com.example.futureplatforms.model.AppConstants
import com.example.futureplatforms.presenter.MainActivityPresenter
import com.example.futureplatforms.repository.MainActivityRepository
import com.example.futureplatforms.util.CalendarUtils
import com.example.futureplatforms.util.UIHelper
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*
import kotlin.math.roundToInt

class MainActivity : AppCompatActivity(), MainActivityContract.View {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        getHelper(this@MainActivity)?.writableDatabase
        setContentView(R.layout.activity_main)
        mPresenter = MainActivityPresenter(MainActivityRepository.getInstance(this)!!, this)
        getMostRecentWeatherInfo()
        fab_sync.setOnClickListener {
            checkLocationPermissions()
        }

    }

    private fun getLocation() {
        try {
            mFusedLocationClient!!.lastLocation
                .addOnSuccessListener(
                    this@MainActivity
                ) { location -> // Got last known location. In some rare situations this can be null.
                    lastLocation = location
                    // if location has been found - then proceed to get the weather details
                    ui = UIHelper(this@MainActivity)
                    ui?.showLoadingDialog("Loading weather... Please wait")
                    mPresenter.getWeatherInfo(
                        lastLocation!!.latitude,
                        lastLocation!!.longitude,
                        AppConstants.APP_ID
                    )
                }
        } catch (e: SecurityException) {
            lastLocation = null
        }
    }

    private fun getMostRecentWeatherInfo() {
        if (!Coord.isTableEmpty(this)) { // check if there is anything to show and if not then display to user that there is no information
            val model = Coord.getWeatherInfoModel(this)
            if (!checkIfDataIsMoreThan24HoursOld(model.lastUpdatedDate!!))
                isWeatherInfoAvailable(false)
            else {
                isWeatherInfoAvailable(true)
                tvCurrentConditions.text = model.currentConditions
                val temperatureString = model.temperature.roundToInt().toString() + "\u2103"
                tvTemperature.text = temperatureString
                val windSpeed = model.windSpeed.roundToInt().toString() + "mph"
                tvWindSpeed.text = windSpeed
                tvWindDirection.text = model.windDirection
                Glide.with(this).load("${AppConstants.ICON_URL}${model.weatherIcon}.png")
                    .into(ivWeather)
                val lastUpdated =
                    "Last Updated Date: " + model.lastUpdatedDate?.let { CalendarUtils.getDate(it) }
                tvLastUpdatedDate.text = lastUpdated
            }
        } else {
            isWeatherInfoAvailable(false)
        }
    }

    private fun checkIfDataIsMoreThan24HoursOld(lastUpdatedDate: Date): Boolean {
        val now = Calendar.getInstance()
        now.time = Date()
        val lastUpdate = Calendar.getInstance()
        lastUpdate.time = lastUpdatedDate
        lastUpdate.add(Calendar.HOUR, 24)
        return lastUpdate >= now
    }

    private fun isWeatherInfoAvailable(isAvailable: Boolean) {
        if (isAvailable) {
            tlTable.visibility = View.VISIBLE
            tvLastUpdatedDate.visibility = View.VISIBLE
            rlNoInfoAvailable.visibility = View.INVISIBLE
        } else {
            tlTable.visibility = View.INVISIBLE
            tvLastUpdatedDate.visibility = View.INVISIBLE
            rlNoInfoAvailable.visibility = View.VISIBLE
        }
    }

    override fun showWeatherInfo() {
        try {
            ui!!.dismissLoadingDialog()
            getMostRecentWeatherInfo()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun showErrorMessage(message: String) {
        try {
            ui!!.dismissLoadingDialog()
            getMostRecentWeatherInfo()
            Toast.makeText(this@MainActivity, message, Toast.LENGTH_LONG).show()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun setPresenter(presenter: MainActivityContract.Presenter?) {
        if (presenter != null)
            mPresenter = presenter
    }

    private fun checkLocationPermissions() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            mFusedLocationClient =LocationServices.getFusedLocationProviderClient(this@MainActivity)
            getLocation()
        } else {
            ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), LOCATION_PERMISSION_CODE)
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == LOCATION_PERMISSION_CODE) {
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this@MainActivity)
                getLocation()
            } else
                Toast.makeText(this@MainActivity, "You need location permission granted for this app to work properly", Toast.LENGTH_LONG).show()
        }
    }

    companion object {
        lateinit var mPresenter: MainActivityContract.Presenter
        private const val LOCATION_PERMISSION_CODE = 1
        private var mFusedLocationClient: FusedLocationProviderClient? = null
        private var lastLocation: Location? = null
        private var ui: UIHelper? = null
    }
}