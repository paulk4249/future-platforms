# Future Platforms Weather App

## Technical Decisions made
This project is written in Kotlin and uses the MVP architecture pattern.
All database and api logic can be found in the Presenter and Repository classes. By doing this it is easier to keep track of
how the project should flow. I decided to use the latitude and longitude as parameters to find out the weather information so the user will be prompted to access location permission
in order to get the correct weather.

##Improvements I could have made
With more time, I would have created a custom class that extends Serializable. That way when I am handling the response from the api I don't need to interrogate the json object - I can simply call
the relevant method from the custom class to get the information. This would be taking full advantage of Retrofit capabilities. I would also take more time in developing tests for the application
so that all aspects are tested and completed successfully.

